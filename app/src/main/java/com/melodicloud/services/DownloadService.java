package com.melodicloud.services;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.melodicloud.util.FileNameUtil;
import com.melodicloud.util.LogUtil;

import java.util.HashMap;

import de.voidplus.soundcloud.Track;

/**
 * Created by Salem on 29-Apr-15.
 */
public class DownloadService {

    static Context context;

    static HashMap<Long, Track> downloadQueue = new HashMap<>();

    public static void init(Context context) {
        DownloadService.context = context;
        // when initialize
        context.registerReceiver(downloadCompleteReceiver, downloadCompleteIntentFilter);
    }

    private static String downloadCompleteIntentName = DownloadManager.ACTION_DOWNLOAD_COMPLETE;
    private static IntentFilter downloadCompleteIntentFilter = new IntentFilter(downloadCompleteIntentName);
    private static BroadcastReceiver downloadCompleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
            if (downloadQueue.get(id) != null) {
                Track track = downloadQueue.get(id);

                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(id);
                Cursor cursor = downloadManager.query(query);

                // it shouldn't be empty, but just in case
                if (!cursor.moveToFirst()) {
                    LogUtil.w("DOWNLOAD", "Download Failed -- 2");
                    return;
                }
                int statusIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (DownloadManager.STATUS_SUCCESSFUL != cursor.getInt(statusIndex)) {
                    LogUtil.w("DOWNLOAD", "Download Failed");
                    return;
                }

                int uriIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);
                String downloadedPackageUriString = cursor.getString(uriIndex);

                afterDownloadAction(downloadedPackageUriString, track);

                return;
            } else {
                LogUtil.w("DOWNLOAD", "Download doesn't exist");
            }
        }
    };

    private static void afterDownloadAction(String downloadedPackageUriString, Track track) {
        LogUtil.d("DOWNLOAD", "Downloaded file to " + downloadedPackageUriString);
    }


    public static void downloadFile(Track track) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);


        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(track.getStreamUrl()));

        // only download via WIFI
        //request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setTitle(track.getTitle());
        //request.setDescription("Downloading a very large zip");

        // we just want to download silently
        request.setVisibleInDownloadsUi(true);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        request.setDestinationInExternalPublicDir("/CloudLiberty", FileNameUtil.sanitizeFileName(track.getTitle()) + ".mp3");

        long downloadID = downloadManager.enqueue(request);

        downloadQueue.put(downloadID, track);
    }

}
