package com.melodicloud.services.requests;

import com.melodicloud.common.Global;
import com.melodicloud.dto.BaseDto;
import com.melodicloud.util.LogUtil;

import java.util.ArrayList;

import de.voidplus.soundcloud.Track;

/**
 * Created by AhmedSalem on 1/25/15.
 */
public class LoadSongsRequest extends BaseRequest<ArrayList<Track>> {


    public LoadSongsRequest() {
        super((Class<ArrayList<Track>>) (Class<?>) ArrayList.class);

    }


    @Override
    public ArrayList<Track> loadDataFromNetwork() throws Exception {

        ArrayList<Track> songs = new ArrayList<>();


        LogUtil.i("loading", "starting loading");
        int offset = 0;
        while (true) {
            ArrayList<Track> songsPage = Global.soundcloud.getMeFavorites(offset, 200);

            if (songsPage == null || songsPage.isEmpty()) {

                break;
            }
            LogUtil.i("loading", "loaded " + songsPage.size());

            songs.addAll(songsPage);

            LogUtil.i("loading", "all is  " + songs.size());

            offset += songsPage.size();

            LogUtil.i("loading", "offset is  " + offset);
        }

        return songs;
    }

    @Override
    public String createCacheKey() {
        return "getFavourites";
    }
}
